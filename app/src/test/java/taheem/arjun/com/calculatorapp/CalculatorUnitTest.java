package taheem.arjun.com.calculatorapp;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * UnitTests for the core Calculator Functionality
 */
public class CalculatorUnitTest {

    @Test
    public void StringAdditionIsCorrect() throws Exception {

        Calculator calculator = new Calculator();
        long result = calculator.add("1", "1");
        assertEquals(2, result);

        long resultForMultipleAdditions = calculator.add("1", "1", "1");
        assertEquals(3, resultForMultipleAdditions);
    }

    @Test
    public void StringAdditionRejectsLetters() throws Exception {

        Calculator calculator = new Calculator();
        long result = calculator.add("0", "r");
        assertEquals(0, result);

        long addOnlyLetters = calculator.add("r", "a");
        assertEquals(0, addOnlyLetters);
    }

    @Test
    public void StringSubtractionIsCorrect() throws Exception {

        Calculator calculator = new Calculator();
        long result = calculator.subtract("1", "1");
        assertEquals(0, result);

        long resultForMultipleSubtraction = calculator.subtract("2", "1", "1");
        assertEquals(0, resultForMultipleSubtraction);
    }

    @Test
    public void StringSubtractionRejectsLetters() throws Exception {

        Calculator calculator = new Calculator();
        long result = calculator.subtract("0", "r");
        assertEquals(0, result);

        long subtractOnlyLetters = calculator.subtract("r", "a");
        assertEquals(0, subtractOnlyLetters);
    }

}