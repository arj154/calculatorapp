package taheem.arjun.com.calculatorapp;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by arjun on 12/07/2017.
 */

public class CalculatorViewModelTests {

    @Test
    public void shouldAppendNumbersToCurrentEquation() throws Exception {

        Calculator calculator = new Calculator();
        CalculatorButtonKey key = new CalculatorButtonKey();
        CalculatorViewModel viewModel = new CalculatorViewModel(calculator, key);

        viewModel.appendToEquation(key.button1);
        viewModel.appendToEquation(key.button2);

        assertEquals("12", viewModel.getCurrentDisplayResult());
    }

    @Test
    public void addNumbersWhenEqualsIsPressed() throws Exception {

        Calculator calculator = new Calculator();
        CalculatorButtonKey key = new CalculatorButtonKey();
        CalculatorViewModel viewModel = new CalculatorViewModel(calculator, key);

        viewModel.appendToEquation(key.button1);
        viewModel.appendToEquation(key.button2);
        viewModel.appendToEquation(key.buttonPlus);
        viewModel.appendToEquation(key.button2);
        viewModel.appendToEquation(key.buttonPlus);
        viewModel.appendToEquation(key.button4);
        viewModel.appendToEquation(key.button5);
        viewModel.appendToEquation(key.buttonEquals);

        assertEquals("59", viewModel.getCurrentDisplayResult());
    }

    @Test
    public void subtractNumbersWhenEqualsIsPressed() throws Exception {

        Calculator calculator = new Calculator();
        CalculatorButtonKey key = new CalculatorButtonKey();
        CalculatorViewModel viewModel = new CalculatorViewModel(calculator, key);

        viewModel.appendToEquation(key.button1);
        viewModel.appendToEquation(key.button2);
        viewModel.appendToEquation(key.buttonMinus);
        viewModel.appendToEquation(key.button2);
        viewModel.appendToEquation(key.buttonMinus);
        viewModel.appendToEquation(key.button5);
        viewModel.appendToEquation(key.buttonEquals);

        assertEquals("5", viewModel.getCurrentDisplayResult());
    }

    @Test
    public void overrideOperatorWhenAdditionalOperatorIsPressed() throws Exception {

        Calculator calculator = new Calculator();
        CalculatorButtonKey key = new CalculatorButtonKey();
        CalculatorViewModel viewModel = new CalculatorViewModel(calculator, key);

        viewModel.appendToEquation(key.button1);
        viewModel.appendToEquation(key.button2);
        viewModel.appendToEquation(key.buttonMinus);
        viewModel.appendToEquation(key.buttonPlus);
        viewModel.appendToEquation(key.button1);
        viewModel.appendToEquation(key.button5);
        assertEquals("12+15", viewModel.getCurrentDisplayResult());
        viewModel.appendToEquation(key.buttonEquals);
        assertEquals("27", viewModel.getCurrentDisplayResult());
    }

    @Test
    public void addToExistingResult() throws Exception {

        Calculator calculator = new Calculator();
        CalculatorButtonKey key = new CalculatorButtonKey();
        CalculatorViewModel viewModel = new CalculatorViewModel(calculator, key);

        viewModel.appendToEquation(key.button1);
        viewModel.appendToEquation(key.button2);
        viewModel.appendToEquation(key.buttonPlus);
        viewModel.appendToEquation(key.button1);
        viewModel.appendToEquation(key.button5);
        viewModel.appendToEquation(key.buttonEquals);
        viewModel.appendToEquation(key.buttonPlus);
        viewModel.appendToEquation(key.button1);
        viewModel.appendToEquation(key.buttonEquals);

        assertEquals("28", viewModel.getCurrentDisplayResult());
    }

    @Test
    public void ensureInitialOperatorIsIgnored() throws Exception {

        Calculator calculator = new Calculator();
        CalculatorButtonKey key = new CalculatorButtonKey();
        CalculatorViewModel viewModel = new CalculatorViewModel(calculator, key);

        viewModel.appendToEquation(key.buttonPlus);
        viewModel.appendToEquation(key.button1);
        viewModel.appendToEquation(key.button5);
        viewModel.appendToEquation(key.buttonPlus);
        viewModel.appendToEquation(key.button1);
        viewModel.appendToEquation(key.buttonEquals);

        assertEquals("16", viewModel.getCurrentDisplayResult());
    }
}
