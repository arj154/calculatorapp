package taheem.arjun.com.calculatorapp;

import java.util.ArrayDeque;
import java.util.Stack;

/**
 * Created by arjun on 02/07/2017.
 */

class Calculator {

    String currentDigit = "";
    Stack<String> operations = new Stack<>();
    ArrayDeque<String> values = new ArrayDeque<>();
    CalculatorButtonKey key = new CalculatorButtonKey();

    public long add(String... numbers) {

        if (numbers.length <= 0
                || !isANumber(numbers[0])) {

            return 0;
        }

        long total = 0;
        for (String number : numbers) {

            if (isANumber(number)) {

                long parsedNumber = Long.parseLong(number);
                total = total + parsedNumber;
            }
        }

        return total;
    }

    private boolean isANumber(String number) {

        String digitRegex = "\\d+|\\+\\d+|[\\-]\\d+";
        return number.matches(digitRegex);
    }

    public long subtract(String... numbers) {

        if (numbers.length <= 0
                || !isANumber(numbers[0])) {

            return 0;
        }

        long result = Long.parseLong(numbers[0]);

        for (int i = 1; i < numbers.length; i++) {

            if (isANumber(numbers[i])) {

                result = result - Long.parseLong(numbers[i]);
            }
        }
        return result;
    }

    public void buildForumula(String value, String lastDigit) {

        if(isAnOperator(value) && lastDigit.isEmpty()) {

            currentDigit = currentDigit + value;
        }
        else if (isLastDigitAnOperator(value, lastDigit)) {

            if (!operations.empty()) {

                operations.pop();
            }
            operations.push(value);
        }
        else if (value.contentEquals("=")) {

            values.push(currentDigit);
            currentDigit = "";

        } else if (value.contentEquals("+")) {

            values.push(currentDigit);
            operations.push("+");
            currentDigit = "";

        } else if (value.contentEquals("-")) {

            values.push(currentDigit);
            operations.push("-");
            currentDigit = "";
        } else {

            currentDigit = currentDigit + value;
        }
    }

    public boolean isLastDigitAnOperator(String value, String lastDigit) {

        return isAnOperator(lastDigit) && isAnOperator(value);
    }

    public boolean isAnOperator(String lastButtonPressed) {

        return lastButtonPressed.contentEquals("+")
                || lastButtonPressed.contentEquals("-")
                || lastButtonPressed.contentEquals("=");
    }

    public String calculateEndResult() {

        for (String operation : operations) {

            if (operation.contentEquals("+")) {

                long result = add(values.pollLast(), values.pollLast());

                values.addLast(result + "");
            } else if (operation.contentEquals("-")) {

                long result = subtract(values.pollLast(), values.pollLast());
                values.addLast(result + "");
            }
        }
        operations.clear();

        currentDigit = (!values.isEmpty()) ? values.pop() : currentDigit;
        return currentDigit;

    }

}
