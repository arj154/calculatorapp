package taheem.arjun.com.calculatorapp;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import taheem.arjun.com.calculatorapp.databinding.ActivityCalculatorBinding;

public class CalculatorActivity extends AppCompatActivity {

    private ActivityCalculatorBinding binding;
    private CalculatorViewModel model;
    private Calculator calculator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_calculator);

        CalculatorButtonKey key = new CalculatorButtonKey();
        if (calculator == null) {

            calculator = new Calculator();
        }

        if (model == null) {

            model = new CalculatorViewModel(calculator, key);
        }
        binding.setCalculatorViewModel(model);
        binding.setKey(key);
    }
}
