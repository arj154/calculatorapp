package taheem.arjun.com.calculatorapp;

/**
 * Created by arjun on 06/07/2017.
 */

public class CalculatorButtonKey {

    public final String button0 = "0";
    public final String button1 = "1";
    public final String button2 = "2";
    public final String button3 = "3";
    public final String button4 = "4";
    public final String button5 = "5";
    public final String button6 = "6";
    public final String button7 = "7";
    public final String button8 = "8";
    public final String button9 = "9";
    public final String buttonPlus = "+";
    public final String buttonMinus = "-";
    public final String buttonEquals = "=";
}
