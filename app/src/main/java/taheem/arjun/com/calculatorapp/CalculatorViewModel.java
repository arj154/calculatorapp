package taheem.arjun.com.calculatorapp;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

/**
 * Created by arjun on 06/07/2017.
 */

public class CalculatorViewModel extends BaseObservable {

    Calculator calculator;
    CalculatorButtonKey key;
    private String currentDisplayResult = "";
    private String lastDigit = "";

    public CalculatorViewModel(Calculator calculator, CalculatorButtonKey key) {

        this.calculator = calculator;
        this.key = key;
    }

    @Bindable
    public String getCurrentDisplayResult() {

        return currentDisplayResult;
    }

    public void appendToEquation(String value) {

        calculator.buildForumula(value, lastDigit);

        if (value.equals(key.buttonEquals)) {

            currentDisplayResult = calculator.calculateEndResult();

        }
        else {

            if (calculator.isLastDigitAnOperator(value, lastDigit)) {

                int displayLength = currentDisplayResult.length();
                int lastDigit =
                        (displayLength > 0) ? displayLength - 1 : 0;

                currentDisplayResult = currentDisplayResult
                        .substring(0, lastDigit);
            }

            currentDisplayResult = currentDisplayResult + value;
            lastDigit = value;
        }

        notifyPropertyChanged(BR.currentDisplayResult);
    }

}
